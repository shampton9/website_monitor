<!DOCTYPE html>
<html>

<head>
    <title>Website Performance</title>

    <link data-require="bootstrap@3.3.7" data-semver="3.3.7" rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
    <script data-require="jquery@*" data-semver="3.1.1" src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script data-require="bootstrap@3.3.7" data-semver="3.3.7" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script data-require="bootstrap.js@*" data-semver="3.3.6" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>

<body>
<div class="container-fluid">

    <nav class="navbar navbar-default">

        <div>

            <div class="navbar-header">

                <button type="button" class="collapsed navbar-toggle" aria-expanded="false">

                    <span class="sr-only">Toggle navigation</span>

                    <span class="icon-bar"></span>

                    <span class="icon-bar"></span>

                    <span class="icon-bar"></span>

                </button>


                <a href="#" class="navbar-brand">Website Performance Stats</a>


            </div>

            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-5">

                <h4 class="navbar-text navbar-right">TifDev</h4>
            </div>

        </div>

    </nav>

    <div class="col-md-8 col-md-offset-2">

<table class="table table-bordered">
        <thead>
            <tr>
                <th>Service</th>
            </tr>
        </thead>
        <tbody>
            <tr <?php 
                $output = shell_exec('ping -c1 logservice.tif-plc.eu');
                if ($output) {echo "class='success'";} else {echo "class='danger'";}
            ?>>
                <td>Structured Log Service</td>
            </tr>
            <tr <?php 
                $output = shell_exec('ping -c1 dataupload.tif-plc.eu');
                if ($output) {echo "class='success'";} else {echo "class='danger'";}
            ?>>
                <td>Document Upload Service</td>
            </tr>
        </tbody>
        </table>

        <?php
        $servername = "localhost";
        $username = "monitor_user";
        $password = "Q94w&)w73!v=MWq(";
        $dbname = "website_monitor";
        $conn = new mysqli($servername, $username, $password, $dbname);

        $sql = "SELECT * FROM monitor";
        $result = $conn->query($sql);


        if ($result->num_rows > 0) {
        // output data of each row
        ?>
        <table class="table table-bordered">
            <thead>
            <tr>
                <th>Website</th><th>Load time 1</th><th>Load time 2</th> <th>Last Updated</th>
            </tr>
            </thead>
            <tbody><?php
            while($row = $result->fetch_assoc()) {
                ?>
                <tr <?php if (($row["loadtime1"] > 5) || ($row["loadtime2"] > 5) || (strtotime($row["updatedDate"]) < strtotime("-30 minutes")))  {echo "class='danger'";} else { echo "class='success'";}?>>
                    <td>
                        <?php echo $row["name"] ?>
                    </td>

                    <td><?php echo $row["loadtime1"] ?></td>
                    <td><?php echo $row["loadtime2"] ?></td>
                    <td><?php echo date( 'd/m/y g:i A', strtotime($row["updatedDate"])); ?> </td>
                </tr>
                <?php
            } ?>
            </tbody>
            <?php
            } else {
                echo "0 results";
            }

            mysqli_close($conn);
            ?>
    </div>
</div>
</body>
</html>
            