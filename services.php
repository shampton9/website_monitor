<!DOCTYPE html>
<html>

<head>
    <title>Service Availability</title>

    <link data-require="bootstrap@3.3.7" data-semver="3.3.7" rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
    <script data-require="jquery@*" data-semver="3.1.1" src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script data-require="bootstrap@3.3.7" data-semver="3.3.7" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script data-require="bootstrap.js@*" data-semver="3.3.6" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container-fluid">
    <nav class="navbar navbar-default">
        <div>
            <div class="navbar-header">
                <button type="button" class="collapsed navbar-toggle" aria-expanded="false">

                    <span class="sr-only">Toggle navigation</span>

                    <span class="icon-bar"></span>

                    <span class="icon-bar"></span>

                    <span class="icon-bar"></span>

                </button>
                <a href="#" class="navbar-brand">Service Availability Stats</a>
            </div>

            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-5">
                <h4 class="navbar-text navbar-right">TifDev</h4>
            </div>

        </div>

    </nav>

    <div class="col-md-8 col-md-offset-2">

<table class="table table-bordered">
        <thead>
            <tr>
                <th>Service</th>
            </tr>
        </thead>
        <tbody>
            <tr <?php 
                $output = shell_exec('ping -c1 logservice.tif-plc.eu');
                if ($output) {echo "class='success'";} else {echo "class='danger'";}
            ?>>
                <td>Structured Log Service</td>
            </tr>
            <tr <?php 
                $output = shell_exec('ping -c1 dataupload.tif-plc.eu');
                if ($output) {echo "class='success'";} else {echo "class='danger'";}
            ?>>
                <td>Document Upload Service</td>
            </tr>
        </tbody>
        </table>
    </div>
</div>
</body>
</html>        